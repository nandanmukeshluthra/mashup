class EntrepreneursController < ApplicationController
  def new
   @user = Entrepreneur.new
   store_location
  end

  def show
    @user = Entrepreneur.find(params[:id])
  end

  def updatepwd
    @user = current_user
  end

  def create
    @entrepreneur = Entrepreneur.new(user_params)
    @user=Users.new
    if @entrepreneur.save
      # Handle a successful save.
      
      if lcurrent_user
         @string=current_user['siteStandardProfileRequest']['url'].scan(/id=+(\d+)/)[0][0]
         @entrepreneur.update_attribute(:linkedin_id,@string)
         @user.update_attribute(:linkedin_id,@string)
      end
      @user.update_attribute(:email,@entrepreneur.email)
      @user.update_attribute(:type,"Entrepreneur")
      @user.save
      sign_in(@entrepreneur,"Entrepreneur")
      flash[:success] = "Welcome to Mashup!!!"
      redirect_to @entrepreneur
    else
      #redirect_back_or "/login"
      render 'new'
    end
  end

  def update
    @user = Entrepreneur.find(params[:id])
    if @user.update_attributes(user_params)
      # Handle a successful update.
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:entrepreneur).permit(:fname, :lname, :email,:password,:password_confirmation)
    end
end
