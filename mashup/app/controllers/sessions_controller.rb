class SessionsController < ApplicationController
  def new
  end

  def create
    @temp = Users.find_by(email: params[:session][:email].downcase)
    if @temp.type=="Student"
       user=Student.find_by(email: params[:session][:email].downcase)
    else
       user=Entrepreneur.find_by(email: params[:session][:email].downcase)
    end
    if user && user.authenticate(params[:session][:password])
      # Sign the user in and redirect to the user's show page.
      sign_in(user,@temp.type)
      redirect_back_or user
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
   sign_out
   redirect_to root_url
  end
end
