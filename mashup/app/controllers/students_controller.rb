class StudentsController < ApplicationController
  before_action :signed_in_user, only: [:edit, :update]

  def new
   @user = Student.new
   store_location
  end

  def show
    @user = Student.find(params[:id])
  end

  def edit
    @user = Student.find(params[:id])
  end

  def updatepwd
    @user = current_user
  end

  def create
    @student = Student.new(user_params)
    @user=Users.new
    if @student.save
      #Handle a successful save.
      if lcurrent_user
         @string=lcurrent_user['siteStandardProfileRequest']['url'].scan(/id=+(\d+)/)[0][0]
         @student.update_attribute(:linkedin_id,@string)
         @user.update_attribute(:linkedin_id,@string)
      end
      @user.update_attribute(:email,@student.email)   
      @user.update_attribute(:type,"Student")
      @user.save
      sign_in(@student,"Student")
      flash[:success] = "Welcome to Mashup!!!"
      redirect_to @student
    else
      #render 'new'
      redirect_to "/students/new"
    end
  end


  def update
    @user = Student.find(params[:id])
    if @user.update_attributes(edit_user_params)
      # Handle a successful update.
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:student).permit(:fname, :lname, :email,:password,:password_confirmation)
    end

    def edit_user_params
      params.require(:student).permit(:fname, :lname, :email,:skills,:bio,:google,:fb,:github,:password,:password_confirmation)
    end
end
